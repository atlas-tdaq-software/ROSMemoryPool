// -*- c++ -*-
/*
  ATLAS TEST Software

  Class: MEMORYPOOLEXCEPTION
  Authors: ATLAS ROS group 	
*/

#ifndef MEMORYPOOLEXCEPTION_H
#define MEMORYPOOLEXCEPTION_H

#include "DFExceptions/ROSException.h"
#include <string>
#include <iostream>

class MemoryPoolException : public ROSException 
{
  
public:
  enum ErrorCode 
  { 
    NOPAGESAVAILABLE,
    NOPAGESALLOCATED,
    ILLEGALPOOLTYPE,
    EXT_ERROR
  };   

  MemoryPoolException(ErrorCode error);
  MemoryPoolException(ErrorCode error, std::string description);
  MemoryPoolException(ErrorCode error, const ers::Context& context);
  MemoryPoolException(ErrorCode error, std::string description, const ers::Context& context);
  MemoryPoolException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);

protected:
  virtual std::string getErrorString(u_int errorId) const;
  virtual ers::Issue * clone() const { return new MemoryPoolException( *this ); }
  static const char * get_uid() {return "ROS::MemoryPoolException";}
  virtual const char* get_class_name() const {return get_uid();}
};

inline MemoryPoolException::MemoryPoolException(MemoryPoolException::ErrorCode error) 
  : ROSException("MemoryPool",error, getErrorString(error)) { }

inline MemoryPoolException::MemoryPoolException(MemoryPoolException::ErrorCode error, std::string description) 
  : ROSException("MemoryPool",error, getErrorString(error),description) { }

inline MemoryPoolException::MemoryPoolException(MemoryPoolException::ErrorCode error, const ers::Context& context)
  : ROSException("MemoryPool",error, getErrorString(error), context) { }

inline MemoryPoolException::MemoryPoolException(MemoryPoolException::ErrorCode error, std::string description, const ers::Context& context)
  : ROSException("MemoryPool",error, getErrorString(error),description, context) { }

inline MemoryPoolException::MemoryPoolException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context) 
     : ROSException(cause, "MemoryPool", error, getErrorString(error), description, context) {}

inline std::string MemoryPoolException::getErrorString(u_int errorId) const 
{
  std::string rc;    
  switch (errorId) 
  {  
    case NOPAGESAVAILABLE:
      rc = "No pages available";
      break;
    case NOPAGESALLOCATED:
      rc = "No pages allocated";
      break;
    case ILLEGALPOOLTYPE:
      rc = "Illegal Pool Type";
      break;
    default:
      rc = "";
      break;
  }
  return rc;
}

#endif //MEMORYPOOLEXCEPTION_H
 
