// -*- c++ -*-  $Id$
/*
  ATLAS ROS Software

  Class: WRAPPERMEMORYPAGE
  Authors: ATLAS ROS group 	
*/

#ifndef WRAPPERMEMORYPAGE_H
#define WRAPPERMEMORYPAGE_H

#include "ROSMemoryPool/MemoryPage.h"
#include "ROSObjectAllocation/FastObjectPool.h"

namespace ROS 
{
  class WrapperMemoryPage : public MemoryPage 
  {
  public:
    WrapperMemoryPage(char * address, u_int size,
		      u_long physicalAddress=0,
		      void (*releaseAction)(MemoryPage *, void *)=0,
		      void *releaseActionParameter=0);
    void * operator new(size_t);
    void operator delete(void *memory);
  };
  
  inline WrapperMemoryPage::WrapperMemoryPage(char * address, u_int size,
					      u_long physicalAddress,
					      void (*releaseAction)(MemoryPage *, void *),
					      void *releaseActionParameter ) 
    : MemoryPage(address,size,physicalAddress,releaseAction,releaseActionParameter) 
    { 
  }
  
  inline void * WrapperMemoryPage::operator new(size_t) 
  {
    return FastObjectPool<WrapperMemoryPage>::allocate(); //This is thread UNSAFE!
  }

  inline void WrapperMemoryPage::operator delete(void * memory) 
  {
    FastObjectPool<WrapperMemoryPage>::deallocate(memory); //This is thread UNSAFE!
  }
}
#endif //WRAPPERMEMORYPAGE_H

