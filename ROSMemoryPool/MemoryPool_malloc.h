// -*- c++ -*-  $Id$
/*
  ATLAS ROS Software

  Class: MEMORYPOOL_MALLOC
  Authors: ATLAS ROS group 	
*/


#ifndef MEMORYPOOL_MALLOC_H
#define MEMORYPOOL_MALLOC_H

#include "MemoryPool.h"

namespace ROS
{
  class MemoryPage;

  /**
   * @stereotype implementationClass 
   */
  class MemoryPool_malloc : public MemoryPool 
  {
  public:         
    MemoryPool_malloc(int nPages, u_int pageSize); 
    virtual ~MemoryPool_malloc();

  private:
    std::vector <MemoryPage *> m_pageVector_malloc;      // the (dynamic) bookkeeping stack
    std::vector <MemoryPage *> m_initPageVector_malloc;  // the initial bookkeeping stack
  };
}
#endif //MEMORYPOOL_MALLOC_H
