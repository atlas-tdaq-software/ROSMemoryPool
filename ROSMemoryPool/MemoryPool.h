// -*- c++ -*-  $Id$
/*
  ATLAS ROS Software

  Class: MEMORYPOOL
  Authors: ATLAS ROS group 	
*/

#ifndef MEMORYPOOL_H
#define MEMORYPOOL_H

#include <vector>
#include "MemoryPoolException.h"

#include "DFDebug/DFDebug.h"

namespace ROS 
{
  class MemoryPage;
  class EventInputManager;

  /**
   * @stereotype interface 
   */
  class MemoryPool
  {
  public:
    enum
    {
      ROSMEMORYPOOL_MALLOC = 1,
      ROSMEMORYPOOL_CMEM_GFP,
      ROSMEMORYPOOL_CMEM_BPA
    };
    friend class MemoryPage;
    friend class EventInputManager;

    MemoryPool(int noPages, u_int pageSize, int noPagesReserved = 0);
    MemoryPage * getPage();
    MemoryPage * getPageNonReserved();
    bool isPageAvailable();
    bool isPageAvailableNonReserved();
    int numberOfFreePages();
    int numberOfFreePagesNonReserved();
    int numberOfPages();
    u_int pageSize();
    void dumpVector();
    void reset(void);
    virtual ~MemoryPool(); 
    
  protected:
    void initVector(std::vector<MemoryPage *> *pageVector);    
    void initVectorInitial(std::vector<MemoryPage *> *pageVector);    
    int m_noPages;
    u_int m_pageSize;
    int m_noPagesReserved;
  
  private:
    u_long getPhysicalAddress(int pageNo);
    MemoryPage * getPagePointer(int pageNo);
    MemoryPage * getPagePointerInitial(int pageNo);
    void freePage(MemoryPage * page); 

    std::vector<MemoryPage *> * m_pageVectorInitial;	// pointer to the initial bookkeeping stack
    std::vector<MemoryPage *> * m_pageVector;		// pointer to the bookkeeping stack
    volatile int m_freeIndex;				// and its index
  };
  
  inline MemoryPool::MemoryPool(int noPages, u_int pageSize, int noPagesReserved) 
    : m_noPages(noPages),
      m_pageSize(pageSize),
      m_noPagesReserved(noPagesReserved),
      m_freeIndex(0) 
  {  
  }
  
  inline int MemoryPool::numberOfPages() 
  {
    return m_noPages;
  }

  inline u_int MemoryPool::pageSize()
  { 
    return m_pageSize;
  }
  
  inline bool MemoryPool::isPageAvailable()
  { 
    return !(m_freeIndex>=m_noPages);
  }

  inline bool MemoryPool::isPageAvailableNonReserved()
  { 
    return !(m_freeIndex>= (m_noPages - m_noPagesReserved));
  }
  
  inline int MemoryPool::numberOfFreePages()
  { 
    return (m_noPages - m_freeIndex);
  }
  
  inline int MemoryPool::numberOfFreePagesNonReserved()
  { 

    int nfree = m_noPages - m_noPagesReserved - m_freeIndex;
    if (nfree >= 0) 
      return nfree;
    else 
      return 0;
  }
  
  inline void MemoryPool::initVector(std::vector<MemoryPage *> *pageVector) 
  {    
    m_pageVector = pageVector;
  }
  
  inline void MemoryPool::initVectorInitial(std::vector<MemoryPage *> *pageVector) 
  {    
    m_pageVectorInitial = pageVector;
  }
  
  inline MemoryPage * MemoryPool::getPage() 
  { 
    if (m_freeIndex >= m_noPages) 
      throw MemoryPoolException(MemoryPoolException::NOPAGESAVAILABLE);

    MemoryPage *rc = (*m_pageVector)[m_freeIndex];
    (*m_pageVector)[m_freeIndex] = 0;
    m_freeIndex++;
    return rc;
  }
  
  inline MemoryPage * MemoryPool::getPageNonReserved() 
  { 
    if (m_freeIndex >= (m_noPages - m_noPagesReserved)) 
      throw MemoryPoolException(MemoryPoolException::NOPAGESAVAILABLE);

    MemoryPage *rc = (*m_pageVector)[m_freeIndex];
    (*m_pageVector)[m_freeIndex] = 0;
    m_freeIndex++;
    return rc;
  }
  
  inline void MemoryPool::freePage(MemoryPage * page) 
  {
    if (m_freeIndex <= 0) 
      throw MemoryPoolException(MemoryPoolException::NOPAGESALLOCATED);

    m_freeIndex--;
    (*m_pageVector)[m_freeIndex] = page;
  }
}


#endif //MEMORYPOOL_H
