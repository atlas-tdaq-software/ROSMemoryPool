// -*- c++ -*-  $Id$
/*
  ATLAS ROS Software

  Class: MEMORYPOOL_CMEM
  Authors: ATLAS ROS group 	
*/


#ifndef MEMORYPOOL_CMEM_H
#define MEMORYPOOL_CMEM_H

#include "MemoryPool.h"

namespace ROS
{
  class MemoryPage;

  /**
   * @stereotype implementationClass 
   */
  class MemoryPool_CMEM : public MemoryPool 
  {
  public:         
    MemoryPool_CMEM(int nPages, u_int pageSize, int bufferType = ROSMEMORYPOOL_CMEM_GFP, int noPagesReserved = 0);
    virtual ~MemoryPool_CMEM();

    u_long getPhysicalBaseAddress(void) const;
    u_long getVirtualBaseAddress(void) const;

  private:
    int m_poolHandle;
    u_long m_vaPool;
    u_long m_paPool;
    int m_bufferType;
    std::vector <MemoryPage *> m_pageVector_CMEM;	// the (dynamic) bookkeeping stack
    std::vector <MemoryPage *> m_initPageVector_CMEM;	// the initial bookkeeping stack
  };

}
#endif //MEMORYPOOL_CMEM_H
