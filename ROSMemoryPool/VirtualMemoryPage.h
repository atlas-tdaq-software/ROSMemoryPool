// -*- c++ -*-  $Id$
/*
  ATLAS ROS Software

  Class: VIRTUALMEMORYPAGE
  Authors: ATLAS ROS group 	
*/

#ifndef VIRTUALMEMORYPAGE_H
#define VIRTUALMEMORYPAGE_H

#include "MemoryPool.h"
#include "ROSMemoryPool/MemoryPage.h"

namespace ROS 
{
  class VirtualMemoryPage : public MemoryPage
  {
  public:
    //BEWARE that there is no consistency check here between the actual size of the
    //original page and the offset and size parameters!!!!
    VirtualMemoryPage(MemoryPage *memoryPage, u_int offset=0) 
	: MemoryPage((char *)(memoryPage->address()) + offset, memoryPage->usedSize()-offset, 0, &ReleaseAction, (void *)memoryPage) 
    { 
       memoryPage->lock();		     
    };

    //BEWARE that there is no consistency check here between the actual size of the
    //original page and the offset and size parameters!!!!
    VirtualMemoryPage(MemoryPage *memoryPage, u_int offset, u_int size) 
	: MemoryPage((char *)(memoryPage->address()) + offset, size, 0, &ReleaseAction, (void *)memoryPage) 
    {
       memoryPage->lock();		     
    };


  private:
    static void ReleaseAction(MemoryPage *, void *); 
    virtual ~VirtualMemoryPage() { };
  };
}
#endif //VIRTUALMEMORYPAGE_H

