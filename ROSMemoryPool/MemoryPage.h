// -*- c++ -*-  $Id$
/*
  ATLAS ROS Software

  Class: MEMORYPAGE
  Authors: ATLAS ROS group 	
*/

#ifndef MEMORYPAGE_H
#define MEMORYPAGE_H

#include "MemoryPool.h"

namespace ROS 
{
  class MemoryPage
  {
  public:
    friend class MemoryPool_malloc;
    friend class MemoryPool_CMEM;
    friend class WrapperMemoryPool;

    void * address() const;
    u_long physicalAddress() const;
    int pageNumber() const;
    u_int usedSize() const;
    u_int capacity() const;
    void lock();
    void free();
    void * reserve(u_int size, u_int minSize = 0);
    int release(u_int size);
    void truncate();
    int referenceCount() const;
    void reset(void);
    void setUserdata(u_int data);
    u_int getUserdata(void);

  protected:  
    MemoryPage(char * address, u_int size, u_long physicalAddress = 0, void(*releaseAction)(MemoryPage *, void *) = 0,
	       void *releaseActionParameter = 0);
    virtual ~MemoryPage() { };
  private:  
    MemoryPage(MemoryPool *memoryPool, char *address, u_int size, int pageNo, u_long physicalAddress = 0);
    MemoryPool * const m_memoryPool;
    void * const m_address;
    const u_long m_physicalAddress;
    const u_int m_size;
    const int m_pageNo;
    u_int m_used;
    u_int m_usable;
    u_int m_lastReserved;
    u_int m_userData;
    int m_nReferences;
    void(*m_releaseAction)(MemoryPage *, void *);
    void *m_releaseActionParameter;
  };
  
  inline MemoryPage::MemoryPage(char *address, u_int size, u_long physicalAddress,
				void(*releaseAction)(MemoryPage *, void *), void *releaseActionParameter) 
    :  m_memoryPool(0),
       m_address(address),
       m_physicalAddress(physicalAddress),
       m_size(size),
       m_pageNo(0),
       m_used(size),
       m_usable(size),
       m_lastReserved(0),
       m_userData(0),
       m_nReferences(1),
       m_releaseAction(releaseAction),
       m_releaseActionParameter(releaseActionParameter)
  {
  }

  inline MemoryPage::MemoryPage(MemoryPool *memoryPool, char *address, u_int size, int pageNo, u_long physicalAddress) 
    :  m_memoryPool(memoryPool),
       m_address(address),
       m_physicalAddress(physicalAddress),
       m_size(size),
       m_pageNo(pageNo),
       m_used(0),
       m_usable(size),
       m_lastReserved(0),
       m_userData(0),
       m_nReferences(1),
       m_releaseAction(0),
       m_releaseActionParameter(0)
  {
  }

  inline void MemoryPage::reset(void) 
  {
    m_used = 0;
    m_usable = m_size;
    m_lastReserved = 0;
    m_nReferences = 1;
  }

  inline void * MemoryPage::address() const 
  {
    return m_address;
  }
  
  inline u_long MemoryPage::physicalAddress() const 
  {
    return m_physicalAddress;
  }
  
  inline u_int MemoryPage::usedSize() const 
  {
    return m_used;
  }
  
  inline u_int MemoryPage::capacity() const 
  {
    return(m_usable-m_used);
  }
  
  inline void MemoryPage::lock()
  {
    truncate();
    m_nReferences++;
  }
  
  inline void MemoryPage::setUserdata(u_int data)
  {
    m_userData = data;
  }
  
  inline u_int MemoryPage::getUserdata(void)
  {
   return m_userData; 
  }

  inline void MemoryPage::free()
  {
    if((--m_nReferences)==0) 
    {
      if(m_memoryPool!=0) 
      { 
	m_used = 0;
	m_usable = m_size;
	m_lastReserved = 0;
	m_nReferences = 1; 
	m_memoryPool->freePage(this); 
      }
      else {
	if(m_releaseAction!=0) 
	  (*m_releaseAction)(this,m_releaseActionParameter);

	//As there is no MemoryPool to do the cleanup.... 
        delete this;
      }
    }
  }

  inline void * MemoryPage::reserve(u_int size, u_int minSize) 
  {
    void *result = 0;
    if(size <= capacity()) 
    {
      result =(void *)(((char *)m_address) + m_used);
      m_used += size;   
      m_lastReserved = size - minSize;
    }
   return result;
  }
  
  inline int MemoryPage::release(u_int size) 
  {
    u_int result = 0;
    if( size <= m_lastReserved) 
    { 
      m_used -= size;   
      m_lastReserved -= size;
      result = size;
    }
    return result;
  }
  
  inline void MemoryPage::truncate() 
  {
    m_lastReserved = 0;
    m_usable = m_used;
  }

  inline int MemoryPage::referenceCount() const 
  {
    return m_nReferences;
  }
  
  inline int MemoryPage::pageNumber() const 
  {
    return m_pageNo;
  }
}
#endif //MEMORYPAGE_H

