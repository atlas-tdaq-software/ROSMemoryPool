// -*- c++ -*-  $Id$
/*
  ATLAS ROS Software

  Class: WRAPPERMEMORYPOOL
  Authors: ATLAS ROS group 	
*/


#ifndef WRAPPERMEMORYPOOL_H
#define WRAPPERMEMORYPOOL_H

#include "MemoryPool.h"

namespace ROS
{
  class MemoryPage;

  /**
   * @stereotype implementationClass 
   */
  class WrapperMemoryPool : public MemoryPool 
  {
  public:         
    WrapperMemoryPool(u_int noPages, u_int pageSize, u_long virtualAddress, u_long physicalAddress, int noPagesReserved = 0);
    virtual ~WrapperMemoryPool();

    u_long getPhysicalBaseAddress(void) const;
    u_long getVirtualBaseAddress(void) const;

  private:
    int m_poolHandle;
    u_long m_vaPool;
    u_long m_paPool;
    int m_bufferType;
    std::vector <MemoryPage *> m_pageVector_;	   // the (dynamic) bookkeeping stack
    std::vector <MemoryPage *> m_initPageVector_;  // the initial bookkeeping stack
  };

}
#endif //WRAPPERMEMORYPOOL_H
