/*
  ATLAS ROS Software

  Class: VIRTUALMEMORYPAGE
  Authors: ATLAS ROS group 	
*/

#include "ROSMemoryPool/VirtualMemoryPage.h"

void ROS::VirtualMemoryPage::ReleaseAction(MemoryPage *mp1, void *mp2) 
{
  MemoryPage *mp = static_cast<MemoryPage*>(mp2);
  mp->free();
}
