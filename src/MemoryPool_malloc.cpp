/*
  ATLAS ROS Software

  Class: MemoryPool_malloc
  Authors: ATLAS ROS group 	
*/

#include "ROSMemoryPool/MemoryPool_malloc.h"
#include "ROSMemoryPool/MemoryPage.h"

using namespace ROS;

MemoryPool_malloc::MemoryPool_malloc(int noPages, u_int pageSize) 
  : MemoryPool(noPages,pageSize) 
{
  DEBUG_TEXT(DFDB_ROSMEMPOOL, 15," MemoryPool_malloc::constructor called");

  m_pageVector_malloc.reserve(noPages);
  m_initPageVector_malloc.reserve(noPages);
  for (int i = 0; i < noPages; i++) 
  {
    MemoryPage * memPage = new MemoryPage(this, new char[pageSize], pageSize, i);
    m_pageVector_malloc.push_back(memPage);
    m_initPageVector_malloc.push_back(memPage);
  }  
  initVector(&m_pageVector_malloc);
  initVectorInitial(&m_initPageVector_malloc);

  DEBUG_TEXT(DFDB_ROSMEMPOOL, 15," MemoryPool_malloc::constructor done");
}


MemoryPool_malloc::~MemoryPool_malloc() 
{
  DEBUG_TEXT(DFDB_ROSMEMPOOL, 15, " MemoryPool_malloc::destructor called");

  // test if pages are in use before deleting?     FIXME
  // delete the "real" pages
  for (int i = 0; i < m_noPages; i++) 
  {
    char* pagePtr = (char*)m_initPageVector_malloc[i]->address();
    delete [] pagePtr;
  }
  DEBUG_TEXT(DFDB_ROSMEMPOOL, 15," MemoryPool_malloc::destructor  Pages deleted");

  // delete the page descriptors
  for (int i = 0; i < m_noPages; i++) 
    delete m_initPageVector_malloc[i];

  DEBUG_TEXT(DFDB_ROSMEMPOOL, 15," MemoryPool_malloc::destructor done");
}

