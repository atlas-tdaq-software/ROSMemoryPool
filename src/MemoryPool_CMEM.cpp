/*
  ATLAS ROS Software

  Class: MemoryPool_CMEM
  Authors: ATLAS ROS group 	
*/

#include "ROSMemoryPool/MemoryPool_CMEM.h"
#include "ROSMemoryPool/MemoryPage.h"
#include "rcc_error/rcc_error.h"
#include "cmem_rcc/cmem_rcc.h"

using namespace ROS;

MemoryPool_CMEM::MemoryPool_CMEM(int noPages, u_int pageSize, int bufferType, int noPagesReserved)
  : MemoryPool(noPages,pageSize,noPagesReserved) 
{
  err_type ret;
  err_str rcc_err_str;

  DEBUG_TEXT(DFDB_ROSMEMPOOL, 15," MemoryPool_CMEM: bufferType = " << bufferType);

  ret = CMEM_Open();
  if (ret != CMEM_RCC_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_ROSMEMPOOL, 5," MemoryPool_CMEM: error from CMEM_Open");
    rcc_error_string(rcc_err_str, ret);
    throw MemoryPoolException(MemoryPoolException::EXT_ERROR, rcc_err_str);
  }

  m_bufferType = bufferType;
  if (m_bufferType != ROSMEMORYPOOL_CMEM_BPA && m_bufferType != ROSMEMORYPOOL_CMEM_GFP)
  {
    DEBUG_TEXT(DFDB_ROSMEMPOOL, 5," MemoryPool_CMEM: illegal buffer type");
    throw MemoryPoolException(MemoryPoolException::ILLEGALPOOLTYPE);
  }
    
  if (m_bufferType == ROSMEMORYPOOL_CMEM_BPA) 
  {
    ret = CMEM_BPASegmentAllocate(noPages * pageSize, (char *)"MEMPOOL", &m_poolHandle);
    DEBUG_TEXT(DFDB_ROSMEMPOOL, 20," MemoryPool_CMEM: CMEM_BPASegmentAllocate returns " << ret);
  }
  else
  {
    ret = CMEM_SegmentAllocate(noPages * pageSize, (char *)"MEMPOOL", &m_poolHandle);
    DEBUG_TEXT(DFDB_ROSMEMPOOL, 20," MemoryPool_CMEM: CMEM_SegmentAllocate returns " << ret);
  }
  
  if (ret != CMEM_RCC_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_ROSMEMPOOL, 5," MemoryPool_CMEM: error from CMEM_(BPA)SegmentAllocate");
    rcc_error_string(rcc_err_str, ret);
    throw MemoryPoolException(MemoryPoolException::EXT_ERROR, rcc_err_str);
  }

  ret = CMEM_SegmentVirtualAddress(m_poolHandle, &m_vaPool);
  if (ret != CMEM_RCC_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_ROSMEMPOOL, 5," MemoryPool_CMEM: error from CMEM_SegmentVirtualAddress");
    rcc_error_string(rcc_err_str, ret);
    throw MemoryPoolException(MemoryPoolException::EXT_ERROR, rcc_err_str);
  }

  ret = CMEM_SegmentPhysicalAddress(m_poolHandle, &m_paPool);
  if (ret != CMEM_RCC_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_ROSMEMPOOL, 5," MemoryPool_CMEM: error from CMEM_SegmentPhysicalAddress");
    rcc_error_string(rcc_err_str, ret);
    throw MemoryPoolException(MemoryPoolException::EXT_ERROR, rcc_err_str);
  }

  char *vaPg = (char*)m_vaPool;
  char *paPg = (char*)m_paPool;

  m_pageVector_CMEM.reserve(noPages);
  m_initPageVector_CMEM.reserve(noPages);
  for (int i = 0; i < noPages; i++) 
  { 
    DEBUG_TEXT(DFDB_ROSMEMPOOL, 20," MemoryPool_CMEM: page = " << i << ", paPg (pci address) = 0x" << HEX((u_long)paPg));
    MemoryPage * memPage = new MemoryPage(this, vaPg, pageSize, i, (u_long)paPg);
    m_pageVector_CMEM.push_back(memPage);
    m_initPageVector_CMEM.push_back(memPage);
    vaPg += pageSize;
    paPg += pageSize;
  }

  initVector(&m_pageVector_CMEM);
  initVectorInitial(&m_initPageVector_CMEM);
  DEBUG_TEXT(DFDB_ROSMEMPOOL, 15," MemoryPool_CMEM: end of function");
}


MemoryPool_CMEM::~MemoryPool_CMEM() 
{  
  err_type ret;
  err_str rcc_err_str;

  DEBUG_TEXT(DFDB_ROSMEMPOOL, 15," ~MemoryPool_CMEM: entered");

  if (m_bufferType == ROSMEMORYPOOL_CMEM_BPA)
    ret = CMEM_BPASegmentFree(m_poolHandle);
  else
    ret = CMEM_SegmentFree(m_poolHandle);
  if (ret != CMEM_RCC_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_ROSMEMPOOL, 5," ~MemoryPool_CMEM: SegmentFree returns " << ret);
    rcc_error_string(rcc_err_str, ret);
    throw MemoryPoolException(MemoryPoolException::EXT_ERROR, rcc_err_str);
  }

  ret = CMEM_Close();
  if (ret != CMEM_RCC_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_ROSMEMPOOL, 5," ~MemoryPool_CMEM: CMEM_Close returns " << ret);
    rcc_error_string(rcc_err_str, ret);
    throw MemoryPoolException(MemoryPoolException::EXT_ERROR, rcc_err_str);
  }

  // test if in use before deleting?   FIXME
  // delete the memory page descriptors
  for (int i = 0; i < m_noPages; i++) 
  {
    delete m_initPageVector_CMEM[i];
  }
  // the page vectors are part of the object ..
  DEBUG_TEXT(DFDB_ROSMEMPOOL, 15," ~MemoryPool_CMEM: end of function");
}


u_long MemoryPool_CMEM::getPhysicalBaseAddress(void) const 
{
  return (u_long ) m_paPool;
}


u_long MemoryPool_CMEM::getVirtualBaseAddress(void) const 
{
  return (u_long ) m_vaPool;
}

