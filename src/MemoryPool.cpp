/*
  ATLAS ROS Software

  Class: MEMORYPOOL
  Authors: ATLAS ROS group 	
*/

#include <iomanip>

#include "ROSMemoryPool/MemoryPool.h"
#include "ROSMemoryPool/MemoryPage.h"

using namespace ROS;

MemoryPage * MemoryPool::getPagePointer(int pageNo) 
{
  return (*m_pageVector)[pageNo];
}


u_long MemoryPool::getPhysicalAddress(int pageNo) 
{
  return (*m_pageVector)[pageNo]->physicalAddress();
}


MemoryPage * MemoryPool::getPagePointerInitial(int pageNo) 
{
  return (*m_pageVectorInitial)[pageNo];
}


void MemoryPool::dumpVector()
{
  std::cout << " Memory page descriptor pointers, size = " << m_pageVector->size() << std::endl;
  std::cout << " zero means: allocated " << std::endl << std::endl;

  std::cout << "   Pointer        real VA    real PA    size  pageNo  refCnt " << std::endl;
  for (std::vector<MemoryPage *>::const_iterator vi = m_pageVector->begin(); vi != m_pageVector->end(); vi++) 
  {
    std::cout << std::hex << "0x" << std::setw(16) << (uintptr_t)*vi << std::dec;
    if (*vi != 0) 
    {
      std::cout << " -> " << std::hex << "0x" << std::setw(16) << (uintptr_t)(*vi)->address() << std::dec;
      std::cout << std::hex << "PCI address = 0x" << std::setw(8) << (*vi)->physicalAddress() << std::dec;
      std::cout << std::setw(8) << (*vi)->usedSize();
      std::cout << std::setw(8) << (*vi)->pageNumber();
      std::cout << std::setw(8) << (*vi)->referenceCount() << std::endl;
    }
    else 
      std::cout << std::endl;
  }
  std::cout << std::endl;
}


void MemoryPool::reset(void) 
{
  for (int i = 0; i < m_noPages; i++) 
  {
    (*m_pageVector)[i] = (*m_pageVectorInitial)[i];
    (*m_pageVector)[i]->reset();
  }
  m_freeIndex = 0;
}

MemoryPool::~MemoryPool() 
{
  //std::cout << " ~MemoryPool " << std::endl;
}

