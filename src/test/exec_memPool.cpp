// $Id$
/****************************************************************/
/*								*/
/*  file exec_memoryPool.cpp					*/
/*								*/
/*  exerciser for the MemoryPool & the MemoryPage Classes	*/
/*  supports both CMEM & malloc					*/
/*								*/
/****************************************************************/

#include "ROSGetInput/get_input.h"

#include "ROSMemoryPool/MemoryPool_CMEM.h"
#include "ROSMemoryPool/MemoryPool_malloc.h"
#include "ROSMemoryPool/MemoryPage.h"
#include "ROSMemoryPool/VirtualMemoryPage.h"

#include "rcc_time_stamp/tstamp.h"

enum {POOLTYPE = 1,
      OPEN,
      GETPAGE,
      GETVIRTUALPAGE,
      GETPAGENONRESERVED,
      ISPAGEAVAILABLE,
      ISPAGEAVAILABLENONRESERVED,
      NUMBEROFFREEPAGES,
      NUMBEROFFREEPAGESNONRESERVED,
      GETNUMBEROFPAGES,
      GETPAGESIZE,
      GETPHYSICALBASEADDRESS,
      GETVIRTUALBASEADDRESS,
      DUMPVECTOR,
      RESET,
      MEMORYPAGEVARS,
      MEMORYPAGERESERVE,
      MEMORYPAGERELEASE,
      MEMORYPAGELOCK,
      MEMORYPAGETRUNCATE,
      MEMORYPAGEFREE,
      MEMORYPAGERESET,
      MEMORYPAGESETUSERDATA,
      MEMORYPAGEGETUSERDATA,
      CLOSE,
      SETDEBUG,
      TIMING,
      TIMING2,
      HELP,
      QUIT = 100
};

using namespace std;
using namespace ROS;

#define MAX_PAGES 10000
#define MAX_J     1000
#define NPOOLS    4


/************/
int main(void)
/************/ 
{
  char option;
  int npages, pagesize, npagesreserved, poolNo, l_npages, quit_flag = 0;
  int poolType = MemoryPool::ROSMEMORYPOOL_CMEM_GFP;		// default
  u_int size, minSize, retSize, userData, dblevel = 0, dbpackage = DFDB_ROSMEMPOOL;
  void * pageAddress;
  tstamp ts1, ts2;
  float delta;
  double nsecpercall;
  MemoryPool *memPool[NPOOLS];
  MemoryPage *memPage;
  MemoryPage *vmemPage;
  MemoryPage *memPage_arr[MAX_PAGES];
  MemoryPool_CMEM *cmem_pool;

  do 
  {
    std::cout << std::endl << std::endl;
    std::cout << " MemoryPool type: " << std::endl;
    std::cout << "   (pool type)                  : " << std::dec << POOLTYPE  << std::endl;
    std::cout << " MemoryPool methods: " << std::endl;
    std::cout << "   open (construct)             : " << std::dec <<  OPEN << std::endl;
    std::cout << "   getPage                      : " << std::dec <<  GETPAGE << std::endl;
    std::cout << "   getVirtualPage               : " << std::dec <<  GETVIRTUALPAGE << std::endl;
    std::cout << "   getPageNonReserved           : " << std::dec <<  GETPAGENONRESERVED << std::endl;
    std::cout << "   isPageAvailable              : " << std::dec <<  ISPAGEAVAILABLE << std::endl;
    std::cout << "   isPageAvailableNonReserved   : " << std::dec <<  ISPAGEAVAILABLENONRESERVED << std::endl;
    std::cout << "   numberOfFreePages            : " << std::dec <<  NUMBEROFFREEPAGES << std::endl;
    std::cout << "   numberOfFreePagesNonReserved : " << std::dec <<  NUMBEROFFREEPAGESNONRESERVED << std::endl;
    std::cout << "   numberOfPages                : " << std::dec <<  GETNUMBEROFPAGES << std::endl;
    std::cout << "   pageSize                     : " << std::dec <<  GETPAGESIZE << std::endl;
    std::cout << "   getphysicalbaseaddress       : " << std::dec <<  GETPHYSICALBASEADDRESS << std::endl;
    std::cout << "   getvirtualbaseaddress        : " << std::dec <<  GETVIRTUALBASEADDRESS << std::endl;
    std::cout << "   dumpVector                   : " << std::dec <<  DUMPVECTOR << std::endl;
    std::cout << "   reset                        : " << std::dec <<  RESET << std::endl;
    std::cout << "   close (destruct)             : " << std::dec <<  CLOSE << std::endl;
    std::cout << std::endl;
    std::cout << " MemoryPage methods: " << std::endl;
    std::cout << "   PAGE VARIABLES               : " << std::dec <<  MEMORYPAGEVARS << std::endl;
    std::cout << "   reserve                      : " << std::dec <<  MEMORYPAGERESERVE << std::endl;
    std::cout << "   release                      : " << std::dec <<  MEMORYPAGERELEASE << std::endl;
    std::cout << "   lock                         : " << std::dec <<  MEMORYPAGELOCK << std::endl;
    std::cout << "   truncate                     : " << std::dec <<  MEMORYPAGETRUNCATE << std::endl;
    std::cout << "   free                         : " << std::dec <<  MEMORYPAGEFREE << std::endl;
    std::cout << "   reset                        : " << std::dec <<  MEMORYPAGERESET << std::endl;
    std::cout << "   setUserdata                  : " << std::dec <<  MEMORYPAGESETUSERDATA << std::endl;
    std::cout << "   getUserdata                  : " << std::dec <<  MEMORYPAGEGETUSERDATA << std::endl;
    std::cout << std::endl;
    std::cout << " Set debug parameters  : " << std::dec <<  SETDEBUG << std::endl;
    std::cout << " TIMING                : " << std::dec <<  TIMING << std::endl;
    std::cout << " TIMING2               : " << std::dec <<  TIMING2 << std::endl;
    std::cout << " HELP                  : " << std::dec <<  HELP << std::endl;
    std::cout << " QUIT                  : " << std::dec <<  QUIT << std::endl;

    std::cout << " ? : ";

    option = getdec();

    switch(option)   
    {
      case POOLTYPE:
	std::cout << " pool type (MALLOC = " << MemoryPool::ROSMEMORYPOOL_MALLOC <<
                             " , CMEM = " << MemoryPool::ROSMEMORYPOOL_CMEM_GFP <<
                             " , BPA = " << MemoryPool::ROSMEMORYPOOL_CMEM_BPA << ") ";
	poolType = getdecd(MemoryPool::ROSMEMORYPOOL_CMEM_GFP);
	break;

      case OPEN: 
	std::cout << " pool # (<" << NPOOLS << ") ";
	poolNo = getdecd(0);

	std::cout << " # pages = ";
	npages = getdecd(8);

	std::cout << " page size = ";
	pagesize = getdecd(1024);

	std::cout << " # pages to reserve = ";
	npagesreserved = getdecd(0);

	try 
	{
	  if (poolType == MemoryPool::ROSMEMORYPOOL_MALLOC)
            memPool[poolNo] = new MemoryPool_malloc(npages, pagesize);
	  else
            memPool[poolNo] = new MemoryPool_CMEM(npages, pagesize, poolType, npagesreserved);
	}
	catch (ROSException& e) 
	{
	  std::cout << " package: " << e.getPackage() << std::endl;
	  std::cout << " error ID: " << e.getErrorId() << std::endl;
	  std::cout << " Description: " << e.getDescription() << std::endl;
	}
	break;

      case GETPAGE: 
	std::cout << " pool # (<" << NPOOLS << ") ";
	poolNo = getdecd(0);

	try 
	{
	  memPage = memPool[poolNo]->getPage();
	  std::cout << " memory page decriptor @ " << memPage <<" :" << std::endl;
	  std::cout << "   virtual address   = " << memPage->address() << std::endl;
	  std::cout << "   physical address  = 0x" << std::hex << memPage->physicalAddress() << std::dec << std::endl;
	  std::cout << "   size (in bytes)   = " << memPage->usedSize() << std::endl;
	}
	catch (ROSException& e) 
	{
	  std::cout << " package: " << e.getPackage() << std::endl;
	  std::cout << " error ID: " << e.getErrorId() << std::endl;
	  std::cout << " Description: " << e.getDescription() << std::endl;
	}
	break;

      case GETVIRTUALPAGE: 
      {  
	std::cout << "Get virtual page from last allocated real page" << std::endl;
	std::cout << " offset ";
	u_int offset = getdecd(0);

	if (offset >= memPage->usedSize()) 
	{
	  std::cout << "Offset is larger than the original page used size" << std::endl;
	  break;
	}  

	std::cout << " size ";
	u_int newSize = getdecd(memPage->usedSize() - offset);

	if (newSize > memPage->usedSize() - offset) 
	{
	  std::cout << "Size is larger than available space" << std::endl;
	  break;
	}  

	try 
	{
	  vmemPage = new VirtualMemoryPage(memPage, offset, newSize);
	  std::cout << " memory page decriptor @ " << vmemPage <<" :" << std::endl;
	  std::cout << "   virtual address   = " << vmemPage->address() << std::endl;
	  std::cout << "   physical address  = 0x" << std::hex << vmemPage->physicalAddress() << std::dec << std::endl;
	  std::cout << "   size (in bytes)   = " << vmemPage->usedSize() << std::endl;
	}
	catch (ROSException& e) 
	{
	  std::cout << " package: " << e.getPackage() << std::endl;
	  std::cout << " error ID: " << e.getErrorId() << std::endl;
	  std::cout << " Description: " << e.getDescription() << std::endl;
	}
      }
      break;

      case GETPAGENONRESERVED:
	std::cout << " pool # (<" << NPOOLS << ") ";
	poolNo = getdecd(0);

	try 
	{
	  memPage = memPool[poolNo]->getPageNonReserved();
	  std::cout << " memory page decriptor @ " << memPage <<" :" << std::endl;
	  std::cout << "   virtual address   = " << memPage->address() << std::endl;
	  std::cout << "   physical address  = 0x" << std::hex << memPage->physicalAddress() << std::dec << std::endl;
	  std::cout << "   size (in bytes)   = " << memPage->usedSize() << std::endl;
	}
	catch (ROSException& e) 
	{
	  std::cout << " package: " << e.getPackage() << std::endl;
	  std::cout << " error ID: " << e.getErrorId() << std::endl;
	  std::cout << " Description: " << e.getDescription() << std::endl;
	}
	break;

      case ISPAGEAVAILABLE:
	std::cout << " pool # (<" << NPOOLS << ") ";
	poolNo = getdecd(0);
	std::cout << " free pages in pool (bool) = " << memPool[poolNo]->isPageAvailable();
	break;

      case ISPAGEAVAILABLENONRESERVED:
	std::cout << " pool # (<" << NPOOLS << ") ";
	poolNo = getdecd(0);
	std::cout << " free non-reserved pages in pool (bool) = " << memPool[poolNo]->isPageAvailableNonReserved();
	break;

      case NUMBEROFFREEPAGES:
	std::cout << " pool # (<" << NPOOLS << ") ";
	poolNo = getdecd(0);
	std::cout << " # free pages in pool = " << memPool[poolNo]->numberOfFreePages();
	break;

      case NUMBEROFFREEPAGESNONRESERVED:
	std::cout << " pool # (<" << NPOOLS << ") ";
	poolNo = getdecd(0);
	std::cout << " # non-reserved free pages in pool = " << memPool[poolNo]->numberOfFreePagesNonReserved();
	break;

      case GETNUMBEROFPAGES:
	std::cout << " pool # (<" << NPOOLS << ") ";
	poolNo = getdecd(0);
	std::cout << " total # pages in pool = " << memPool[poolNo]->numberOfPages();
	break;

      case GETPAGESIZE:
	std::cout << " pool # (<" << NPOOLS << ") ";
	poolNo = getdecd(0);
	std::cout << " page size  = " << memPool[poolNo]->pageSize();
	break;

      case GETPHYSICALBASEADDRESS:
	std::cout << " pool # (<" << NPOOLS << ") ";
	poolNo = getdecd(0);
	cmem_pool = dynamic_cast<MemoryPool_CMEM *>(memPool[poolNo]);
	std::cout << " physical base address  = 0x" << std::hex << cmem_pool->getPhysicalBaseAddress() << std::dec;
	break;

      case GETVIRTUALBASEADDRESS:
	std::cout << " pool # (<" << NPOOLS << ") ";
	poolNo = getdecd(0);
	cmem_pool = dynamic_cast<MemoryPool_CMEM *>(memPool[poolNo]);
	std::cout << " virtual base address  = 0x" << std::hex << cmem_pool->getVirtualBaseAddress() << std::dec;
	break;

      case DUMPVECTOR:
	std::cout << " pool # (<" << NPOOLS << ") ";
	poolNo = getdecd(0);
	memPool[poolNo]->dumpVector();
	break;

      case RESET:
	std::cout << " pool # (<" << NPOOLS << ") ";
	poolNo = getdecd(0);
	memPool[poolNo]->reset();
	break;

      case MEMORYPAGEVARS:
	std::cout << "  variables of the last page (descriptor) allocated via getPage  = " << memPage << std::endl;
	std::cout << "   virtual address   = 0x" << std::hex << memPage->address() << std::endl << std::dec;
	std::cout << "   physical address  = 0x" << std::hex << memPage->physicalAddress() << std::endl << std::dec;
	std::cout << "   size (in bytes)   = " << memPage->usedSize() << std::endl;
	std::cout << "   capacity          = " << memPage->capacity() << std::endl;
	std::cout << "   reference count   = " << memPage->referenceCount() << std::endl;
	std::cout << "   page number       = " << memPage->pageNumber() << std::endl;
	break;

      case MEMORYPAGERESERVE:
	std::cout << "  last page (descriptor) allocated via getPage  = " << memPage << std::endl;
	std::cout << " size (in bytes) ";
	size = getdecd(100);
	minSize = 0;	// ?
	pageAddress = memPage->reserve(size, minSize);
	std::cout << " got address = " << pageAddress << std::endl;
	std::cout << " check page status with PAGE VARIABLES .. " << std::endl;
	break;

      case MEMORYPAGERELEASE:
	std::cout << "  last page (descriptor) allocated via getPage  = " << memPage << std::endl;
	std::cout << " size (in bytes) ";
	size = getdecd(100);
	retSize = memPage->release(size);
	std::cout << " released " << retSize << " bytes " << std::endl;
	std::cout << " check page status with PAGE VARIABLES .. " << std::endl;
	break;

      case MEMORYPAGELOCK:
	std::cout << "  last page (descriptor) allocated via getPage  = " << memPage << std::endl;
	memPage->lock();
	std::cout << " check page status with PAGE VARIABLES .. " << std::endl;
	break;

      case MEMORYPAGETRUNCATE:
	std::cout << "  last page (descriptor) allocated via getPage  = " << memPage << std::endl;
	memPage->truncate();
	std::cout << " check page status with PAGE VARIABLES .. " << std::endl;
	break;

      case MEMORYPAGEFREE:
	std::cout << "  page (descriptor) = ";
	memPage = (MemoryPage *)gethex();
	memPage->free();
	break;

      case MEMORYPAGERESET:
	memPage->reset();
	break;

      case MEMORYPAGESETUSERDATA:
	std::cout << "  user data  = ";
	userData = getdecd(100);
	memPage->setUserdata(userData);
	break;

      case MEMORYPAGEGETUSERDATA:
	userData = memPage->getUserdata();
	std::cout << "  user data  = " << userData << std::endl;
	break;

      case CLOSE:
	std::cout << " pool # (<" << NPOOLS << ") ";
	poolNo = getdecd(0);
        delete memPool[poolNo];
	break;

      case SETDEBUG:
	std::cout << "Enter the debug level: " << std::endl;
	dblevel = getdecd(dblevel);
	std::cout << "Enter the debug package: " << std::endl;
	dbpackage = getdecd(dbpackage);
	DF::GlobalDebugSettings::setup(dblevel, dbpackage);
	break;

      case TIMING:
	TS_OPEN(10000, TS_DUMMY);
	std::cout << " TIMING is using Pool # 0 " << std::endl;
	std::cout << " # pages (<" << npages << ") :" << std::endl;
	l_npages = getdec();
	ts_clock(&ts1);

	for (int j = 0; j < MAX_J; j++) 
	{	// just to make time longer .. 
	  for (int i = 0; i < l_npages; i++) 
	  {
	    try 
	    {
              memPage_arr[i] = memPool[0]->getPage();
	    }
	    catch (ROSException& e) 
	    {
              std::cout << " package: " << e.getPackage() << std::endl;
              std::cout << " error ID: " << e.getErrorId() << std::endl;
              std::cout << " Description: " << e.getDescription() << std::endl;
	    }
          }
	  for (int i = 0; i < l_npages; i++) 
	    (memPage_arr[i])->free();		// using the MemoryPage free ...
	}

	ts_clock(&ts2);
	delta = ts_duration(ts1, ts2);
	std::cout << " # get/free calls = " << l_npages * MAX_J << std::endl;
	std::cout << " # seconds = " << delta << std::endl;
	nsecpercall = (delta / (l_npages * MAX_J)) * 1000000000;
	std::cout << " nanosecs/page for get/free = " << nsecpercall << std::endl;
	break;

      case TIMING2:
	TS_OPEN(10000, TS_DUMMY);
	std::cout << " TIMING is using Pool # 0 " << std::endl;
	std::cout << " # pages (<" << npages << ") :" << std::endl;
	l_npages = getdec();
	ts_clock(&ts1);

	for (int j = 0; j < MAX_J; j++) 
	{	// just to make time longer .. 
	  for (int i = 0; i < l_npages; i++)  
	  {
	    try 
	    {
              memPage_arr[i] = memPool[0]->getPage();
	    }
	    catch (ROSException& e) 
	    {
              std::cout << " package: " << e.getPackage() << std::endl;
              std::cout << " error ID: " << e.getErrorId() << std::endl;
              std::cout << " Description: " << e.getDescription() << std::endl;
	    }

	    (memPage_arr[i])->free();
	  }
	}

	ts_clock(&ts2);
	delta = ts_duration(ts1, ts2);
	std::cout << " # get/free calls = " << l_npages * MAX_J << std::endl;
	std::cout << " # seconds = " << delta << std::endl;
	nsecpercall = (delta / (l_npages * MAX_J)) * 1000000000;
	std::cout << " nanosecs/page for get/free = " << nsecpercall << std::endl;
	break;

      case HELP:
	std::cout <<  " Exerciser program for the MemoryPool & MemoryPage classes." << std::endl;
	std::cout <<  " In addition the performance of the time critical functions" << std::endl;
	std::cout <<  " can be measured" << std::endl;
	std::cout <<  " The results of the MemoryPage methods can be chacked via the" << std::endl;
	std::cout <<  " PAGE VARIABLES entry" << std::endl;
	std::cout <<  " Several memory pools can be built and operated upon." << std::endl;
	std::cout <<  " The commands  in lower case letters correspond one-to-one to" << std::endl;
	std::cout <<  " the methods in the MemoryPool & MemoryPage classes." << std::endl;
	break;

      case QUIT:
	quit_flag = 1;
	break;

      default:
	std::cout <<  "not implemented yet" << std::endl;

    } /*main switch */
  } while (quit_flag == 0);

  return 0;
}
