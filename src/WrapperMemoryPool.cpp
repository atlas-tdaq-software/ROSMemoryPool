/*
  ATLAS ROS Software

  Class: WrapperMemoryPool
  Authors: ATLAS ROS group 	
*/

#include "ROSMemoryPool/WrapperMemoryPool.h"
#include "ROSMemoryPool/MemoryPage.h"
#include "rcc_error/rcc_error.h"
#include "cmem_rcc/cmem_rcc.h"

using namespace ROS;

WrapperMemoryPool::WrapperMemoryPool(u_int noPages,
				     u_int pageSize,
				     u_long virtualAddress, 
				     u_long physicalAddress,
				     int noPagesReserved)
  : MemoryPool(noPages,pageSize,noPagesReserved),
    m_vaPool(virtualAddress),
    m_paPool(physicalAddress) 
{
  char *vaPg = (char*)m_vaPool;
  char *paPg = (char*)m_paPool;

  m_pageVector_.reserve(noPages);
  m_initPageVector_.reserve(noPages);
  for (u_int i = 0; i < noPages; i++) 
  {
    MemoryPage * memPage = new MemoryPage(this, vaPg, pageSize, i, (u_long)paPg);
    m_pageVector_.push_back(memPage);
    m_initPageVector_.push_back(memPage);
    vaPg += pageSize;
    paPg += pageSize;
  }

  initVector(&m_pageVector_);
  initVectorInitial(&m_initPageVector_);
}

WrapperMemoryPool::~WrapperMemoryPool() 
{
  // test if in use before deleting?   FIXME
  // delete the memory page descriptors
  for (int i = 0; i < m_noPages; i++) 
    delete m_initPageVector_[i];
}

u_long WrapperMemoryPool::getPhysicalBaseAddress(void) const 
{
  return (u_long ) m_paPool;
}

u_long WrapperMemoryPool::getVirtualBaseAddress(void) const 
{
  return (u_long ) m_vaPool;
}

