
tdaq_package()

tdaq_add_library(ROSMemoryPool src/*.cpp
                 INCLUDE_DIRECTORIES PRIVATE Boost
                 LINK_LIBRARIES cmem_rcc rcc_error DFExceptions)

tdaq_add_executable(exec_memPool src/test/exec_memPool.cpp LINK_LIBRARIES ROSMemoryPool rcc_time_stamp getinput)
tdaq_add_executable(exec_wmemPool src/test/exec_wmemPool.cpp LINK_LIBRARIES ROSMemoryPool rcc_time_stamp getinput)
